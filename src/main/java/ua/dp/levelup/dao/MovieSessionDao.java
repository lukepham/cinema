package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.MovieSession;

import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Shegeda on 23.06.17.
 */
public interface MovieSessionDao {

    void createMovieSession(MovieSession session);

    MovieSession getMovieSessionById(long sessionId);

    List<MovieSession> getAllMovieSessions();

    List<MovieSession> getMovieSessionsForToday();

    List<MovieSession> getAllMovieSessionByDate(Date date);

    List<MovieSession> getMovieSessionsByDate(Date date);
}
