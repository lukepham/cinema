package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.User;
import ua.dp.levelup.dao.UserDao;

import java.util.*;

/**
 * Created by java on 20.06.2017.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    private Map<Long, User> userByIdMap = new HashMap<>();
    private Map<String, Long> idByEmailMap = new HashMap<>();

    @Autowired
    private HibernateTemplate template;

    public void init() {
        List<User> users = Arrays.asList(
                new User("user1@yopmail.com","ff","Alan","Pugachov"),
                new User("user2@yopmail.com","dd", "Bob", "Dilan")
        );

       /* for (User u : users) {
            u.increaseBalance(random.nextInt(100) + 25);

            createUser(u);
        }*/

        System.out.println("Init say Hello");


    }

    @Override
    public List<User> getAllUsers() {
        return (List<User>) userByIdMap.values();
    }

    @Override
    public User getUserByEmail(String email) {
        Long userId = idByEmailMap.get(email);
        User user = userByIdMap.get(userId);
        if (null != user) return user;

        String query = "from User u where u.email = :email";
        List users = (List<User>) template.findByNamedParam(query, "email", email);
        if (users.size() == 0) return null;

        user = (User) users.get(0);
        userByIdMap.put(user.getId(), user);
        idByEmailMap.put(user.getEmail(), user.getId());
        return user;
    }

    @Override
    public User getUserByConfirmCode(String confirmCode) {
        String query = "from User u where u.confirmCode = :confirmCode";
        List users = (List<User>) template.findByNamedParam(query, "confirmCode", confirmCode);
        if (users.size() != 0) {
            return (User) users.get(0);
        } else {
            return null;
        }
    }

    @Override
    public User getUserById(Long id) {
        return userByIdMap.get(id);
    }

    @Override
    public void createUser(User user) {
        template.save(user);
        userByIdMap.put(user.getId(), user);
        idByEmailMap.put(user.getEmail(), user.getId());
    }

    @Override
    public void deleteUser(User user) {
        userByIdMap.remove(user.getId());
        idByEmailMap.remove(user.getEmail());
    }

    @Override
    public void updateUser(User user) {
        userByIdMap.put(user.getId(), user);
        idByEmailMap.put(user.getEmail(), user.getId());
        template.update(user);
    }
}
