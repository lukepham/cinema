package ua.dp.levelup.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "tickets")
@AllArgsConstructor
@Entity
@Table(name = "MOVIE_SESSION_ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    @JsonBackReference
    private List<Ticket> tickets = new ArrayList<>();
    private double totalPrice;
    private Long clientId;

    @Temporal(TemporalType.DATE)
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "orderDate", nullable = false)
    private Date orderDate = new Date();

    public Order(double totalPrice, Long clientId) {
        this.totalPrice = totalPrice;
        this.clientId = clientId;
        this.orderDate = new Date();
    }

    public Order(Long clientId) {
        this.clientId = clientId;
        this.orderDate = new Date();
    }

    public Order(Long orderId, List<Ticket> tickets, Long clientId) {
        this.orderId = orderId;
        this.clientId = clientId;

        for (Ticket ticket : tickets) addTicket(ticket);
    }

    public Order() {
        this.orderDate = new Date();
    }

    public void addTicket(Ticket t) {
        this.tickets.add(t);
        this.totalPrice += t.getPrice();
        t.setOrder(this);
    }
}