package ua.dp.levelup.core.model;

public enum SeatType {

    COMFORT(1),
    LUX(2);

    private int type;

    SeatType(int type) {
        this.type = type;
    }



    public int getType() {
        return type;
    }

    }