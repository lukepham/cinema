package ua.dp.levelup.core.model.filters;
import org.springframework.stereotype.Component;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;

import java.util.Comparator;

@Component
public class Filters {
    public Comparator<MovieSession> sessionComparator() {
        return new Comparator<MovieSession>() {
            @Override
            public int compare(MovieSession ms1, MovieSession ms2) {
                int dateSessions = Long.compare(ms1.getSessionStartDate().getTime(), (ms2.getSessionStartDate().getTime()));
                int timeSessions = Long.compare(ms1.getSessionStartTime().getTime(), (ms2.getSessionStartTime().getTime()));

                if (dateSessions == 0 && timeSessions == 0) return 0;
                else if (dateSessions == 0) return timeSessions;
                else return dateSessions;
            }
        };
    }
    public Comparator<Film> filmComparator() {
        return new Comparator<Film>() {
            @Override
            public int compare(Film film1, Film film2) {
                int nameFilm = film1.getName().compareToIgnoreCase(film2.getName());
                if (nameFilm == 0) return 0;
                else return nameFilm;
            }
        };
    }
}
