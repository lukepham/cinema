package ua.dp.levelup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Hall;
import ua.dp.levelup.dao.HallDao;
import ua.dp.levelup.service.HallService;

import java.util.List;

@Service("hallService")
public class HallServiceImpl implements HallService {

    private HallDao hallDao;

    @Autowired
    public void setHallDao(HallDao hallDao) {
        this.hallDao = hallDao;
    }

    @Override
    public void createHall(Hall hall) {
        hallDao.createHall(hall);
    }

    @Override
    public void updateHall(Hall hall) {
        hallDao.updateHall(hall);
    }

    @Override
    public void deleteHall(Hall hall) {
        hallDao.deleteHall(hall);
    }

    @Override
    public Hall getHallById(Long id) {
        return hallDao.getHallById(id);
    }

    @Override
    public Hall getHallByNumber(int hallNumber) {
        return hallDao.getHallByNumber(hallNumber);
    }

    @Override
    public List<Hall> getAllHalls() {
        return hallDao.getAllHalls();
    }
}
