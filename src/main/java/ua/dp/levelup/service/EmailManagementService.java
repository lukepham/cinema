package ua.dp.levelup.service;

import ua.dp.levelup.core.model.message.MessageForUsers;
import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.User;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Queue;


public interface EmailManagementService {

    void sendEmail(String recipient, String subject, String emailMessage) throws MessagingException;

    void sendNotificationEmailAfterTicketBuy(String recipient, Order order) throws MessagingException, IOException;

    Queue<MessageForUsers> getMessageQueue();

    void addMessageToQueue(MessageForUsers messages);

}
