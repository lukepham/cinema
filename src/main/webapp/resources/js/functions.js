function Add() {
    $("#tblData tbody").append("<tr>" +
        "<td style='display:none'><input type='text' disabled/></td>" +
        "<td><input type='text'/></td>" +
        "<td><input type='text'/></td>" +
        "<td><input type='text'/></td>" +
        "<td><a href='#' class='btnSave'>SAVE</a>&nbsp;<a href='#' class='btnDelete'>DELETE</a></td>" +
        "</td>" + "</tr>");
    $(".btnSave").bind("click", Save);
    $(".btnDelete").bind("click", Delete);
};

function Save() {
    var par = $(this).parent().parent(); //tr
    var tdName = par.children("td:nth-child(2)");
    var tddescription = par.children("td:nth-child(3)");
    var tdduration = par.children("td:nth-child(4)");
    var tdButtons = par.children("td:nth-child(5)");
    var name = tdName.children("input[type=text]").val();
    var description = tddescription.children("input[type=text]").val();
    var duration = tdduration.children("input[type=text]").val();
    var imageName = "none";

    let createFilmController = 'http://localhost:8080/film/create';
    let film = {name, description, duration, imageName};

    fetch(createFilmController, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(film)
    })
        .then(response => {
            if (response.ok) {
                alert("Your request successful. Film " + name + " - created!");
                tdName.html(name);
                tddescription.html(description);
                tdduration.html(duration);
                tdButtons.html("<a href='#' class='btnDelete'>DELETE</a>&nbsp;<a href='#' class='btnEdit'>EDIT</a>");
            } else {
                alert("Your request ignored. Film" + name + " - was not added!");
            }
        }).catch(error => console.error(error));
}

function Edit() {
    var par = $(this).parent().parent(); //tr
    var tdName = par.children("td:nth-child(2)");
    var tddescription = par.children("td:nth-child(3)");
    var tdduration = par.children("td:nth-child(4)");
    var tdButtons = par.children("td:nth-child(5)");
    tdName.html("<input type='text' value='" + tdName.html() + "'/>");
    tddescription.html("<input type='text' value='" + tddescription.html() + "'/>");
    tdduration.html("<input type='text' value='" + tdduration.html() + "'/>");
    tdButtons.html("<a href='#' class='btnUpdate'>UPDATE</a>");
    $(".btnSave").bind("click", Save);
    $(".btnUpdate").bind("click", Update);
    $(".btnEdit").bind("click", Edit);
    $(".btnDelete").bind("click", Delete);
}


function Delete() {
    var par = $(this).parent().parent();
    var tdId = par.children("td:nth-child(1)");
    var filmIdToDelete = tdId.html();
    var formData = new FormData();
    formData.append("filmId", filmIdToDelete);
    let deleteFilmController = 'http://localhost:8080/film/delete';

    fetch(deleteFilmController, {
        method: 'POST',
        body: formData
    }).then(function (response) {
        console.log(response => response);
    });
    par.remove();
}

function Update() {
    var par = $(this).parent().parent(); //tr
    var tdId = par.children("td:nth-child(1)");
    var tdName = par.children("td:nth-child(2)");
    var tddescription = par.children("td:nth-child(3)");
    var tdduration = par.children("td:nth-child(4)");
    var tdButtons = par.children("td:nth-child(5)");

    var filmId = tdId.text();
    var name = tdName.children("input[type=text]").val();
    var description = tddescription.children("input[type=text]").val();
    var duration = tdduration.children("input[type=text]").val();

    var imageName = "none";

    let updateFilmController = 'http://localhost:8080/film/update';
    let film = {filmId, name, description, duration, imageName};
    console.log(JSON.stringify(film));

    fetch(updateFilmController, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(film)
    })
        .then(response => {
            if (response.ok) {
                alert("Your request successful. Film " + name + " - update!");
                tdName.html(tdName.children("input[type=text]").val());
                tddescription.html(tddescription.children("input[type=text]").val());
                tdduration.html(tdduration.children("input[type=text]").val());
                tdButtons.html("<a href='#' class='btnDelete'>DELETE</a>&nbsp;<a href='#' class='btnEdit'>EDIT</a>");
            } else {
                alert("Your request ignored. Film" + name + " - was not updated!");
            }
        }).catch(error => console.error(error));
}

$(function () {
    $(".btnEdit").bind("click", Edit);
    $(".btnDelete").bind("click", Delete);
    $(".btnUpdate").bind("click", Update);
    $("#btnAdd").bind("click", Add);
});
