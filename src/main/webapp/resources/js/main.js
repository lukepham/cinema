document.getElementById("sign-in").addEventListener('click', addLoginForm);
document.getElementById("authorization").addEventListener('click', addAuthForm);

function addLoginForm() {
    if (document.getElementById("login-form").hasAttribute('hidden')) {
        changeForm("auth-form");
        document.getElementById("login-form").removeAttribute('hidden');
    }
    else {
        document.getElementById("form-container").setAttribute('hidden', '');
        document.getElementById("login-form").setAttribute('hidden', '');
    }
}
function addAuthForm() {
    if(document.getElementById("auth-form").hasAttribute('hidden')){
        changeForm("login-form");
        document.getElementById("auth-form").removeAttribute('hidden')
    }
    else {
        document.getElementById("form-container").setAttribute('hidden', '');
        document.getElementById("auth-form").setAttribute('hidden', '');
    }
}
function changeForm(data) {
    document.getElementById(data).setAttribute('hidden','');
    document.getElementById("form-container").removeAttribute("hidden");
}
function removeForm(){
    document.getElementById("form-container").setAttribute('hidden','');
    document.getElementById("auth-form").setAttribute('hidden', '');
    document.getElementById("login-form").setAttribute('hidden', '');
}

function validateEmail(event){

    event.preventDefault();
    var email = document.getElementById("new-email");
    var emailText = email.value;

    if(emailText.length === 0 || emailText.includes(" ") || emailText.indexOf("@") === -1){
        email.classList.add('red-border');
     }
    else {
        validateLogin;
    }
}

function validateLogin(event){

    event.preventDefault();
    var login = document.getElementById("new-login");
    var loginText = login.value;

    if(loginText.length === 0 || loginText.includes(" ")){
        login.classList.add('red-border');
    }
    else {
        sendUser;
    }
}

function resetEmailField(){
    document.getElementById("new-email").classList.remove('red-border');
}

function resetLoginField(){
    document.getElementById("new-login").classList.remove('red-border');
}