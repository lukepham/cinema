document.getElementById("cancelAddHall").addEventListener('click', cancelAddHall);
function showAddHall() {
    document.getElementById('add-hall').removeAttribute('hidden');
    document.getElementById('add-hall-form').setAttribute('hidden','');
    document.getElementById('hide-hall-form').removeAttribute('hidden');
}
function hideHallForm() {
    document.getElementById('add-hall').setAttribute('hidden','');
    document.getElementById('add-hall-form').removeAttribute('hidden');
    document.getElementById('hide-hall-form').setAttribute('hidden', '');
    document.getElementById('cancelAddHall').setAttribute('hidden','');
}

function cancelAddHall(){
    let firstElement = document.getElementById('add-rows').firstElementChild;
    document.getElementById('add-rows').innerHTML = '';
    document.getElementById('add-rows').appendChild(firstElement);
    document.getElementById('add-rows').setAttribute('hidden','');
    document.getElementById('create-seats').setAttribute('hidden','');
    document.getElementById('hallName').innerHTML = "HALLS";
    hideHallForm();
}

function createHall() {

    let hallNumber = document.getElementById("hall-number").value;
    let quantityRows = document.getElementById("quantity-rows").value;
    let rows = [];
    let input;
    let blockOFRows = document.getElementById('add-rows');
    let paragraf = blockOFRows.firstElementChild;

    for(let i = 0; i < quantityRows-1; i++){
        rows[i] = (paragraf.cloneNode(true));
        rows[i].firstElementChild.innerHTML = 'Quantity seats in Row '+(i+2)+' - ';
        rows[i].querySelector('[name="type-row"]').setAttribute('row', (i+2).toString());
        input = rows[i].querySelector('[name="row"]');
        input.setAttribute('number-row', (i+2).toString());
        blockOFRows.appendChild(rows[i]);
    }
    document.getElementById("add-hall").setAttribute('hidden', '');
    document.getElementById("add-rows").removeAttribute('hidden');
    document.getElementById('cancelAddHall').removeAttribute('hidden');
    document.getElementById('create-seats').removeAttribute('hidden');
    document.getElementById('hide-hall-form').setAttribute('hidden', '');
    document.getElementById('hallName').innerHTML = "HALL - " + hallNumber;
    return false;
}

function createSeats() {

    let quantityRows = document.getElementById("quantity-rows").value;
    let hallNumber = parseInt(document.getElementById("hall-number").value);
    let blockOFRows = document.getElementById('add-rows');
    let seatsType = blockOFRows.getElementsByTagName("select");
    let createHall = 'http://localhost:8080/hall/create';
    let getAllHalls = 'http://localhost:8080/hall';
    let quantitySeatsInRow = 1;
    let seatType = "COMFORT";
    let hall = {hallNumber};
    let seats = [];
    let rows = [];

    for(let i = 0; i < quantityRows; i++) {
        rows[i] = new Object({rowNumber: i + 1});
        quantitySeatsInRow = blockOFRows.querySelector('[number-row=' + '"' + (i + 1) + '"' + ']').value;
        seatType =  searchSelected(seatsType[i]);
        seats[i] = [];
        for (let j = 0; j < quantitySeatsInRow; j++) {
            seats[i][j] = new Object({seatNumber: j+1, seatType: seatType});
        }
        rows[i].seats = seats[i];
        function searchSelected(data) {
            return data.options[data.selectedIndex].value;
        }
    }
    hall.rows = rows;
    fetch(createHall, {
        method : "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(hall)
    })
        .then(response => {
            if(response.ok){
                alert("Your request successful. Hall with number: " + hallNumber + " - created");
                setTimeout(document.location.assign(getAllHalls), 500);
            }else { alert("Your request ignored. Hall with number: " + hallNumber + " - already exists" );
                    cancelAddHall();}
        }).catch(error => console.error(error));

    return false;
}